import React from 'react';
import StringListInput from './StringListInput';

class RuleClauseInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            clause: props.clause,
            index: props.index,
        };
    }

    render() {
        const { clause, index } = this.state;

        return <div className='RuleClauseInput SubForm'>
            <div className='SubField'>
                <div className='Label'>If</div>
                <input className='Input'
                       defaultValue={clause.if}
                       onChange={event => this.on_change_if(event)}/>
            </div>
            <div className='SubField'>
                <div className='Label'>Changes</div>
                <StringListInput key_prefix='Rule_Clause_Input_Changes_Key_'
                                 list={clause.changes || []}
                                 on_change={list => this.on_change_changes(list)}/>
            </div>
            <div className='SubField'>
                <div className='Label'>Exists</div>
                <StringListInput key_prefix='Rule_Clause_Input_Exists_Key_'
                                 list={clause.changes || []}
                                 on_change={list => this.on_change_changes(list)}/>
            </div>
            <div className='SubField'>
                <div className='Label'>When</div>
                <select className='Input'
                        defaultValue={clause.when}
                        onChange={event => this.on_change_when(event)}>
                    <option value='on_success'>On success</option>
                    <option value='on_failure'>On failure</option>
                    <option value='manual'>Manual</option>
                    <option value='delayed'>Delayed</option>
                    <option value='always'>Always</option>
                </select>
            </div>
            <div className='SubField'>
                <div className='Label'>Start in</div>
                <input className='Input'
                       defaultValue={clause.start_in}
                       disabled={clause.when !== 'delayed'}
                       onChange={event => this.on_change_start_in(event)}/>
            </div>
            <div className='SubField'>
                <div className='Label'>Allow failure</div>
                <select className='Input'
                        onChange={event => this.on_change_allow_failure(event)}>
                    <option>False</option>
                    <option>True</option>
                </select>
            </div>
            <div className='SubField'>
                <div className='Label'/>
                <button onClick={() => this.on_save()}>Save clause</button>
            </div>
            <div className='SubField'>
                <div className='Label'/>
                <button onClick={() => this.on_delete()}>Delete clause</button>
            </div>
        </div>;
    }

    on_change_if(event) {
        const { clause } = this.state;
        clause.if = `"${event.target.value}"`;
        this.setState({ clause });
    }

    on_change_changes(list) {
        const { clause } = this.state;
        clause.changes = list;
        this.setState({ clause });
    }

    on_change_exists(list) {
        const { clause } = this.state;
        clause.exists = list;
        this.setState({ clause });
    }

    on_change_when(event) {
        const { clause } = this.state;
        clause.when = event.target.value;
        this.setState({ clause });
    }

    on_change_start_in(event) {
        const { clause } = this.state;
        clause.start_in = event.target.value;
        this.setState({ clause });
    }

    on_change_allow_failure(event) {
        const { clause } = this.state;
        clause.allow_failure = event.target.value === 'True';
        console.log(clause);
        this.setState({ clause });
    }

    on_save() {
        const { clause, index } = this.state;
        if (clause.if === '') delete clause.if;
        if (clause.changes && clause.changes.length === 0) delete clause.changes;
        if (clause.exists && clause.exists.length === 0) delete clause.exists;
        if (clause.start_in === '') delete clause.start_in;
        this.props.on_save(clause, index);
    }

    on_delete() {
        this.props.on_delete(this.state.index);
    }
}

export default RuleClauseInput;
