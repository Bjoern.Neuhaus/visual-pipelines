import React from 'react';

import './StringListInput.css';

class StringListInput extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [...props.list],
            edit_index: null,
        };
    };

    render() {
        const { key_prefix, placeholder_add, value_prefix, disabled } = this.props;
        const { edit_index, list } = this.state;
        const mapper = (item, index) => {
            let element;
            if (edit_index === index) {
                element = <input autoFocus
                                 className='Input'
                                 defaultValue={item}
                                 onKeyUp={event => this.on_edit_item_key_up(event, index)}
                                 onBlur={() => this.setState({ edit_index: null })}
                                 type='text'/>;
            }
            else {
                element = <a className='Value'
                             onClick={() => this.setState({ edit_index: index })}>
                    {value_prefix || '- '}
                    {item}
                </a>;
            }
            return <li key={`${key_prefix}_StringList_Item_${index}`}>
                {element}
                <button onClick={() => this.delete_by_index(index)}>Delete</button>
            </li>;
        };
        const list_items = list.map(mapper);
        return <div className='StringListInput'>
            <ul>{list_items}</ul>
            <input className='Input'
                   disabled={disabled}
                   onKeyUp={event => this.on_add_item_key_up(event)}
                   placeholder={placeholder_add || 'Add'}
                   type='text'/>
        </div>;
    }

    on_edit_item_key_up(event, index) {
        const { list } = this.state;
        list[index] = event.target.value;
        this.setState({ list });
        this.props.on_change(list);
        if (event.key === 'Enter') event.target.blur();
    }

    on_add_item_key_up(event) {
        const { list } = this.state;
        const new_value = event.target.value;

        const save_keys = ['Enter'];

        if (new_value !== '' && save_keys.indexOf(event.key) > -1) {
            list.push(new_value);
            event.target.value = '';
            this.setState({ list }, (new_state) => {
                this.props.on_change(list);
            });
        }
    }

    delete_by_index(index) {
        if (window.confirm('Confirm to delete')) {
            const { list } = this.state;
            list.splice(index, 1);
            this.setState({ list }, (new_state) => {
                this.props.on_change(list);
            });
        }
    }
}

export default StringListInput;
