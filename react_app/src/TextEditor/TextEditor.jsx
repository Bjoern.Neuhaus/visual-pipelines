import React from 'react';
import yaml from 'js-yaml';

import AceEditor from 'react-ace';
import 'ace-builds/src-noconflict/mode-yaml';
import 'ace-builds/src-noconflict/theme-eclipse';
import 'ace-builds/src-noconflict/theme-iplastic';

import model, { pipeline_text_editor_save, text_editor_set_readonly, text_editor_toggle_minimized } from '../model';

import './TextEditor.css';

let changed_value = false;

const on_change = value => {
    changed_value = value;
    window.render_app();
};

const on_save = () => {
    pipeline_text_editor_save(changed_value);
    changed_value = false;
};

const TextEditor = () => {
    const { text_editor_minimized, text_editor_readonly } = model;
    const value = yaml.safeDump(model.pipeline, { indent: 2 });
    if (text_editor_minimized) return null;
    else {
        const footer = <div className='grid_cols_auto'>
            <button disabled={!changed_value}
                    onClick={on_save}>
                Save
            </button>
            <button onClick={() => text_editor_toggle_minimized()}>
                Hide
            </button>
        </div>;

        let value_prop = undefined;
        let default_value_prop = undefined;
        let on_change_prop = undefined;
        let on_focus_prop = undefined;
        let read_only_prop;

        if (text_editor_readonly) {
            value_prop = value;
            on_focus_prop = () => text_editor_set_readonly(false);
            read_only_prop = true;
        }
        else {
            default_value_prop = value;
            on_change_prop = on_change;
            read_only_prop = false;
        }
        return <div id='TextEditor'>
            <AceEditor defaultValue={default_value_prop}
                       height='100%'
                       fontSize={15}
                       mode='yaml'
                       name='AceEditor'
                       onChange={on_change_prop}
                       onFocus={on_focus_prop}
                       readOnly={read_only_prop}
                       setOptions={{
                           fontFamily: 'Source Code Pro, monospace',
                           useSoftTabs: true,
                           navigateWithinSoftTabs: true,
                       }}
                       theme='eclipse'
                       value={value_prop}
                       width='100%'/>
            {footer}
        </div>;
    }
};

export default TextEditor;
