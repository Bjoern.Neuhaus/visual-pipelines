import React from 'react';

import BaseTabs from './BaseTabs';

import './TopTabs.css';

const TopTabs = ({ tabs }) => <BaseTabs tabs={tabs} tab_location='top'/>;

export default TopTabs;
