import React from 'react';
import classnames from 'classnames';

import model, { pipelib_toggle_minimized, text_editor_toggle_minimized } from './model';

import JobEditor from './Jobs/JobEditor';
import Modal from './Containers/Modal';
import PipeLib from './PipeLib/PipeLib';
import TextEditor from './TextEditor/TextEditor';
import VisualEditor from './VisualEditor/VisualEditor';

import './App.css';

function App() {
    return <div id='App'>
        <div id='AppLeft'>
            <button disabled>Default</button>
            <button disabled>Includes</button>
            <button disabled>Mixins</button>
            <button disabled>Workflows</button>
        </div>
        <VisualEditor/>
        <div id='AppRight'>
            <button className={classnames({ Active: !model.text_editor_minimized })}
                    onClick={() => text_editor_toggle_minimized()}>
                Source
            </button>
            <button className={classnames({ Active: !model.pipelib_minimized })}
                    onClick={() => pipelib_toggle_minimized()}>
                Library
            </button>
        </div>
        <Modal/>
        <JobEditor/>
        <TextEditor/>
        <PipeLib base_url='https://cors-anywhere.herokuapp.com/https://gitlab.com/sri-at-gitlab/projects/pipe-lib'/>
    </div>;
}

export default App;
