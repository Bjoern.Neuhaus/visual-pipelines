import React from 'react';
import ReactDOM from 'react-dom';

import App from './App';
import model, { close_all_modals } from './model';

import './index.css';

const dom_root = document.getElementById('root');

window.render_app = () => {
    console.log('Render', model);
    ReactDOM.render(<App model={model}/>, dom_root);
};

window.addEventListener('load', window.render_app);

window.addEventListener('keydown', event => {
    if (event.key === 'Escape') {
        const input_nodes = ['select', 'input', 'textarea'];
        const target_node = event.target.nodeName.toLowerCase();
        if (input_nodes.indexOf(target_node) > -1) event.target.blur();
        else close_all_modals();
    }
});

// Hot Module Replacement (HMR) - Remove this snip pet to remove HMR.
// Learn more: https://www.snowpack.dev/#hot-module-replacement
if (import.meta.hot) import.meta.hot.accept();
