import React from 'react';
import StringListInput from '../../Inputs/StringListInput';
import model, { pipeline_set_job_after_script } from '../../model';

class AfterScriptField extends React.Component {
    render() {
        const job = model.pipeline[model.job_editor_name];

        return <div className='Field'>
            <div className='Label'>
                After Script
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#before_script-and-after_script'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Used to define the command that will be run after each job, including failed ones.
                </div>
            </div>
            <StringListInput key_prefix='After_Script'
                             list={job.after_script || []}
                             on_change={value => this.on_change(value)}
                             placeholder_add='Add after script'
                             value_prefix='$ '/>
        </div>;
    }

    on_change(value) {
        pipeline_set_job_after_script(model.job_editor_name, value);
    }
}

export default AfterScriptField;
