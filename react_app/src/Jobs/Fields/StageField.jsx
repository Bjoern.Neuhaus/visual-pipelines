import React from 'react';
import model, { get_stages, pipeline_set_job_stage } from '../../model';

class StageField extends React.Component {
    render() {
        const job = model.pipeline[model.job_editor_name];
        const stages = get_stages();
        const mapper = (stage, index) => <option key={`option_${index}`} value={index}>{stage}</option>;
        const options = stages.map(mapper);
        const value = model.pipeline.stages.indexOf(job.stage);

        return <label className='Field'>
            <div className='Label'>
                Stage
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#stage'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Defined per-job and relies on stages which is defined globally. It allows to group jobs into
                    different stages, and jobs of the same stage are executed in parallel
                </div>
            </div>
            <div>
                <select className='Input'
                        defaultValue={value}
                        onChange={event => this.on_change(event)}
                        size={stages.length}>
                    {options}
                </select>
            </div>
        </label>;
    }

    on_change(event) {
        pipeline_set_job_stage(model.job_editor_name, event.target.value);
    }
}

export default StageField;
