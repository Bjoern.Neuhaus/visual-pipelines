import React from 'react';
import model, { pipeline_set_job_resource_group } from '../../model';

class ResourceGroupField extends React.Component {
    render() {
        const job = model.pipeline[model.job_editor_name];
        return <label className='Field'>
            <div className='Label'>
                Resource Group
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#resource_group'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Used to ensure that the Runner won't run certain jobs simultaneously
                </div>
            </div>
            <div>
                <input className='Input'
                       defaultValue={job.resource_group}
                       onChange={event => this.on_change(event)}
                       type='text'/>
            </div>
        </label>;
    }

    on_change(event) {
        const job = model.pipeline[model.job_editor_name];
        const current_value = job.resource_group;
        const new_value = event.target.value;
        if (new_value && new_value !== current_value) {
            pipeline_set_job_resource_group(model.job_editor_name, new_value);
        }
        else if (!new_value) {
            pipeline_set_job_resource_group(model.job_editor_name, null);
        }
    }
}

export default ResourceGroupField;
