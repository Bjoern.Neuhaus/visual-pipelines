import React from 'react';
import model, { get_all_job_names, pipeline_set_job_needs } from '../../model';

class NeedsField extends React.Component {
    render() {
        const selected_needs = model.pipeline[model.job_editor_name].needs || [];
        const jobs = get_all_job_names();
        const mapper = job => {
            if (job === model.job_editor_name) return null;
            return <option key={`job_${job}`}>{job}</option>;
        };
        const options = jobs.map(mapper);
        return <label className='Field'>
            <div className='Label'>
                Needs
                <a href='https://docs.gitlab.com/ee/ci/yaml/#needs'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Define job dependencies enabling out-of-order job execution
                </div>
            </div>
            <select className='Input'
                    defaultValue={selected_needs}
                    multiple
                    onChange={event => this.on_change(event)}
                    size={Math.max(6, options.length)}>{options}</select>
        </label>;
    }

    on_change(event) {
        const selected_values = [...event.target.selectedOptions].map(o => o.value);
        pipeline_set_job_needs(model.job_editor_name, selected_values);
    }
}

export default NeedsField;
