import classnames from 'classnames';
import React from 'react';
import model, { pipeline_update_job_name } from '../../model';

class NameField extends React.Component {
    constructor(props) {
        super(props);
        this.state = { error_message: null };
    }

    render() {
        return <label className={classnames({ Field: true, Error: this.state.error_message })}>
            <div className='Label'>
                Name
                <div className='Hint'>Unique name for this job</div>
            </div>
            <div>
                <input autoFocus
                       className='Input'
                       defaultValue={model.job_editor_name}
                       onChange={event => this.on_change(event)}
                       onFocus={event => event.target.select()}
                       type='text'/>
                <div className='ErrorMessage'>{this.state.error_message}</div>
            </div>
        </label>;
    }

    on_change(event) {
        const current_name = model.job_editor_name;
        const new_name = event.target.value;
        if (!new_name) {
            this.setState({ error_message: 'Job name required' });
        }
        else if (model.pipeline[new_name]) {
            this.setState({ error_message: 'Job already exists with that name' });
        }
        else if (new_name !== current_name) {
            this.setState({ error_message: null });
            pipeline_update_job_name(current_name, new_name);
        }
    }
}

export default NameField;
