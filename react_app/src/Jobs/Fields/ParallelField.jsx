import React from 'react';
import model, { pipeline_set_job_parallel } from '../../model';

class ParallelField extends React.Component {
    render() {
        const value = model.pipeline[model.job_editor_name].parallel || '';

        return <label className='Field'>
            <div className='Label'>
                Parallel
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#parallel'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Allows you to configure how many instances of a job to run in parallel. This value has to be greater
                    than or equal to two (2) and less than or equal to 50.
                </div>
            </div>
            <div>
                <input className='Input'
                       defaultValue={value}
                       min={2}
                       max={50}
                       onChange={event => this.on_change(event)}
                       placeholder='Number between 2 and 50'
                       type='number'/>
            </div>
        </label>;
    }

    on_change(event) {
        const value = (event.target.value && parseInt(event.target.value)) || null;
        pipeline_set_job_parallel(model.job_editor_name, value);
    }
}

export default ParallelField;
