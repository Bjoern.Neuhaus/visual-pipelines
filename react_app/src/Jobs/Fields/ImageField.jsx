import React from 'react';
import model, { pipeline_set_job_image } from '../../model';

class ImageField extends React.Component {
    render() {
        const job = model.pipeline[model.job_editor_name];

        return <label className='Field'>
            <div className='Label'>
                Image
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#image'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Used to specify a Docker image to use for the job.
                </div>
            </div>
            <div>
                <input className='Input'
                       defaultValue={job.image}
                       onChange={event => this.on_change(event)}
                       onFocus={event => event.target.select()}
                       placeholder='Docker image name (optionally full path to Docker registry and image)'
                       type='text'/>
            </div>
        </label>;
    }

    on_change(event) {
        const job = model.pipeline[model.job_editor_name];
        const current_value = job.image;
        const new_value = event.target.value;
        if (new_value && new_value !== current_value) {
            pipeline_set_job_image(model.job_editor_name, new_value);
        }
    }
}

export default ImageField;
