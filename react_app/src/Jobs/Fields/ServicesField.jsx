import React from 'react';
import StringListInput from '../../Inputs/StringListInput';
import model, { pipeline_set_job_services } from '../../model';

class ServicesField extends React.Component {
    render() {
        const job = model.pipeline[model.job_editor_name];

        return <div className='Field'>
            <div className='Label'>
                Services
                <a href='https://docs.gitlab.com/ee/ci/yaml/README.html#services'
                   target='_blank'>Docs</a>
                <div className='Hint'>
                    Used to specify a service Docker image, linked to a base image specified in image
                </div>
            </div>
            <StringListInput key_prefix='Services'
                             list={job.services || []}
                             on_change={value => this.on_change(value)}
                             placeholder_add='Add Docker image name (optionally full path to Docker registry and image)'
                             value_prefix=''/>
        </div>;
    }

    on_change(value) {
        pipeline_set_job_services(model.job_editor_name, value);
    }
}

export default ServicesField;
