import React from 'react';
import { drag_data_separator } from '../Stages/Stages';
import { job_editor_set_name, set_drag_type } from '../model';
import './Jobs.css';

export const JobItem = ({ name, config }) => {
    const on_drag_start = event => {
        event.stopPropagation();
        event.dataTransfer.setData('text/plain', `job${drag_data_separator}${name}`);
        event.dataTransfer.dropEffect = 'move';
        set_drag_type('job');
        setTimeout(() => {
            const drop_zone_left_right = document.querySelectorAll('.DropLeft, .DropRight');
            drop_zone_left_right.forEach(drop_zone => drop_zone.style.zIndex = 3);
        }, 0);
    };
    return <div className='JobItem'
                draggable={true}
                onDragStart={on_drag_start}
                onClick={() => job_editor_set_name(name)}>
        <div className='Title'>{name}</div>
        <div className='Image'>{config.image}</div>
    </div>;
};
