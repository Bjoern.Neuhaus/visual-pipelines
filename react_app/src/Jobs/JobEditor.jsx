import React from 'react';

import AfterScriptField from './Fields/AfterScriptField';
import ArtifactsField from './Fields/ArtifactsField';
import BeforeScriptField from './Fields/BeforeScriptField';
import CacheField from './Fields/CacheField';
import CoverageField from './Fields/CoverageField';
import EnvironmentField from './Fields/EnvironmentField';
import ExtendsField from './Fields/ExtendsField';
import ImageField from './Fields/ImageField';
import InterruptibleField from './Fields/InterruptibleField';
import NameField from './Fields/NameField';
import NeedsField from './Fields/NeedsField';
import ParallelField from './Fields/ParallelField';
import ReleaseField from './Fields/ReleaseField';
import ResourceGroupField from './Fields/ResourceGroupField';
import RetryField from './Fields/RetryField';
import RulesField from './Fields/RulesField';
import ScriptField from './Fields/ScriptField';
import ServicesField from './Fields/ServicesField';
import SideTabs from '../Containers/SideTabs';
import StageField from './Fields/StageField';
import TimeoutField from './Fields/TimeoutField';
import TriggerField from './Fields/TriggerField';
import VariablesField from './Fields/VariablesField';

import './JobEditor.css';

import model, { job_editor_set_name, pipeline_delete_job } from '../model';

const on_delete_click = () => {
    const confirm = window.confirm(`Confirm to delete job "${model.job_editor_name}"`);
    if (confirm) pipeline_delete_job(model.job_editor_name);
};

class JobEditor extends React.Component {
    render() {
        const hide_job_editor = !model.job_editor_name;
        if (hide_job_editor) return null;

        const tabs = [
            {
                title: 'Core',
                content: <SideTabs tabs={[
                    {
                        title: 'Name',
                        content: <section className='Section'><NameField/></section>,
                    },
                    {
                        title: 'Stage',
                        content: <section className='Section'><StageField/></section>,
                    },
                    {
                        title: 'Image',
                        content: <section className='Section'><ImageField/></section>,
                    },
                    {
                        title: 'Script',
                        content: <section className='Section'><ScriptField/></section>,
                    },
                ]}/>,
            },
            {
                title: 'Execution',
                content: <SideTabs tabs={[
                    {
                        title: 'Before Script',
                        content: <section className='Section'><BeforeScriptField/></section>,
                    },
                    {
                        title: 'Script',
                        content: <section className='Section'><ScriptField/></section>,
                    },
                    {
                        title: 'After Script',
                        content: <section className='Section'><AfterScriptField/></section>,
                    },
                    {
                        title: 'Parallel',
                        content: <section className='Section'><ParallelField/></section>,
                    },
                ]}/>,
            },
            {
                title: 'Context',
                content: <SideTabs tabs={[
                    {
                        title: 'Environment',
                        content: <section className='Section'><EnvironmentField/></section>,
                    },
                    {
                        title: 'Extends',
                        content: <section className='Section'><ExtendsField/></section>,
                    },
                    {
                        title: 'Image',
                        content: <section className='Section'><ImageField/></section>,
                    },
                    {
                        title: 'Resource Group',
                        content: <section className='Section'><ResourceGroupField/></section>,
                    },
                    {
                        title: 'Services',
                        content: <section className='Section'><ServicesField/></section>,
                    },
                    {
                        title: 'Variables',
                        content: <section className='Section'><VariablesField/></section>,
                    },
                ]}/>,
            },
            {
                title: 'Control Flow',
                content: <SideTabs tabs={[
                    {
                        title: 'Interruptible',
                        content: <section className='Section'>
                            <InterruptibleField/>
                        </section>,
                    },
                    {
                        title: 'Needs',
                        content: <section className='Section'>
                            <NeedsField/>
                        </section>,
                    },
                    {
                        title: 'Retry',
                        content: <section className='Section'>
                            <RetryField/>
                        </section>,
                    },
                    {
                        title: 'Rules',
                        content: <section className='Section'>
                            <RulesField/>
                        </section>,
                    },
                    {
                        title: 'Timeout',
                        content: <section className='Section'>
                            <TimeoutField/>
                        </section>,
                    },
                    {
                        title: 'Trigger',
                        content: <section className='Section'>
                            <TriggerField/>
                        </section>,
                    },
                ]}/>,
            },
            {
                title: 'Output',
                content: <SideTabs tabs={[
                    {
                        title: 'Cache',
                        content: <section className='Section'>
                            <CacheField/>
                        </section>,
                    },
                    {
                        title: 'Coverage',
                        content: <section className='Section'>
                            <CoverageField/>
                        </section>,
                    },
                    {
                        title: 'Artifacts',
                        content: <section className='Section'>
                            <ArtifactsField/>
                        </section>,
                    },
                    {
                        title: 'Release',
                        content: <section className='Section'>
                            <ReleaseField/>
                        </section>,
                    },
                ]}/>,
            },
        ];

        const side_tabs = <SideTabs tabs={tabs}/>;

        return <div id='JobEditor'>
            <div className='Header'>
                <div className='Title'>{model.job_editor_name}</div>
            </div>
            <div className='Main'
                 style={{ maxHeight: `${window.innerHeight - 250}px` }}>
                {side_tabs}
            </div>
            <div className='ActionBar'>
                <button className='Danger' onClick={() => on_delete_click()}>Delete</button>
                <div>Changes saved automatically</div>
                {/*<div>Undo with Ctrl+Z or Cmd+Z</div>*/}
                <button onClick={() => job_editor_set_name(null)}>Close</button>
            </div>
        </div>;
    }
}

export default JobEditor;
