import React from 'react';

import Stages from '../Stages/Stages';

import { text_editor_set_readonly } from '../model';

const VisualEditor = () => {
    return <div id='VisualEditor'
                onClick={() => text_editor_set_readonly(true)}>
        <Stages/>
    </div>;
};

export default VisualEditor;
