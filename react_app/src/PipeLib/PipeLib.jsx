import React from 'react';

import model, { close_all_modals, get_all_job_names, get_stages, pipeline_text_editor_save } from '../model';
import Library from './Library';
import TopTabs from '../Containers/TopTabs';

import './PipeLib.css';
import yaml from 'js-yaml';

const options = { headers: { origin: 'pipelib' } };

class PipeLib extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            pipelines_library: [],
            pipelines_loading: true,
            pipelines_error: false,
            jobs_library: [],
            jobs_loading: true,
            jobs_error: false,
        };
        this.load_pipelines();
        this.load_jobs();
    }

    render() {
        if (model.pipelib_minimized) return null;

        const { pipelines_library, pipelines_loading, pipelines_error } = this.state;
        const { jobs_library, jobs_loading, jobs_error } = this.state;

        let pipelines_content = null;
        if (pipelines_error) pipelines_content = <div>Unable to load pipelines</div>;
        else if (pipelines_loading) pipelines_content = <div>Loading...</div>;
        else pipelines_content = <Library items={pipelines_library}
                                          on_click_import={item => this.import_pipeline(item)}/>;

        let jobs_content = null;
        if (jobs_error) jobs_content = <div>Unable to load jobs</div>;
        else if (jobs_loading) jobs_content = <div>Loading...</div>;
        else jobs_content = <Library items={jobs_library}
                                     on_click_import={item => this.import_job(item)}/>;

        return <div id='PipeLib'>
            <TopTabs tabs={[
                {
                    title: 'Pipelines',
                    content: pipelines_content,
                },
                {
                    title: 'Jobs',
                    content: jobs_content,
                },
            ]}/>
        </div>;
    }

    load_pipelines() {
        const { base_url } = this.props;
        const url_pipelines_library = `${base_url}/-/raw/master/pipelines_library.json`;
        fetch(url_pipelines_library, options)
            .then(res => res.json())
            .then(res => {
                const pipelines_library = res;
                const pipelines_loading = false;
                const pipelines_error = false;
                this.setState({ pipelines_library, pipelines_loading, pipelines_error });
            })
            .catch(error => {
                this.setState({ pipelines_loading: false, pipelines_error: true });
            });
    }

    load_jobs() {
        const { base_url } = this.props;
        const url_jobs_library = `${base_url}/-/raw/master/jobs_library.json`;
        fetch(url_jobs_library, options)
            .then(res => res.json())
            .then(res => {
                const jobs_library = res;
                const jobs_loading = false;
                const jobs_error = false;
                this.setState({ jobs_library, jobs_loading, jobs_error });
            })
            .catch(() => this.setState({ jobs_loading: false, jobs_error: true }));
    }

    import_pipeline(item) {
        if (window.confirm('Confirm to import')) {
            const { base_url } = this.props;
            const url = `${base_url}/-/raw/master/${item.path}`;
            fetch(url, options)
                .then(res => res.text())
                .then(text => {
                    pipeline_text_editor_save(text);
                    close_all_modals();
                });
        }
    }

    import_job(item) {
        if (window.confirm('Confirm to import')) {
            const { base_url } = this.props;
            const url = `${base_url}/-/raw/master/${item.path}`;
            fetch(url, options)
                .then(res => res.text())
                .then(text => {
                    const original = yaml.safeDump(model.pipeline, { indent: 2 });
                    pipeline_text_editor_save(`${original}\n\n${text}`);
                    const all_job_names = get_all_job_names();
                    const last_job_name = all_job_names[all_job_names.length - 1];
                    const job = model.pipeline[last_job_name];
                    const stages = get_stages();
                    if (stages.indexOf(job.stage) === -1) {
                        model.pipeline.stages.push(job.stage);
                    }
                    close_all_modals();
                });
        }
    }
}

export default PipeLib;
