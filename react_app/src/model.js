import yaml from 'js-yaml';
import random_words from 'random-words';

const model = {
    pipeline: { stages: [] },
    text_editor_minimized: true,
    text_editor_readonly: true,
    drag_type: null,
    job_editor_name: null,
    pipelib_minimized: true,
};

const lint_render = () => {
    // const body = JSON.stringify({ content: JSON.stringify(model.pipeline) });
    // const options = {
    //     body,
    //     cache: 'no-cache',
    //     headers: { 'Content-Type': 'application/json' },
    //     method: 'post',
    //     mode: 'no-cors',
    // };
    // fetch('https://www.gitlab.com/api/v4/ci/lint', options)
    //     .then(response => response.json())
    //     .then(response => {
    model.text_editor_readonly = true;
    window.render_app();
    // });
};

export const close_all_modals = () => {
    model.text_editor_minimized = true;
    model.pipelib_minimized = true;
    model.job_editor_name = null;
    lint_render();
};

export const get_stages = () => model.pipeline.stages;

export const get_all_job_names = () => {
    const all_keys = Object.keys(model.pipeline);
    const literals = ['default', 'stages', 'include', 'workflow', 'cache', 'image'];
    const is_job = key => key[0] !== '.' && literals.indexOf(key) === -1;
    return all_keys.filter(is_job);
};

export const get_all_mixins = () => {
    const all_keys = Object.keys(model.pipeline);
    const is_mixin = key => key[0] === '.';
    return all_keys.filter(is_mixin);
};

export const get_jobs_for_stage = stage => {
    const all_keys = Object.keys(model.pipeline);
    const is_job = key => model.pipeline[key].stage === stage && key[0] !== '.' && key !== 'default';
    const job_keys = all_keys.filter(is_job);
    return job_keys.map(key => ({ name: key, config: model.pipeline[key] }));
};

export const get_drag_type = () => model.drag_type;

export const set_drag_type = value => model.drag_type = value;

export const text_editor_toggle_minimized = () => {
    model.text_editor_minimized = !model.text_editor_minimized;
    if (!model.text_editor_minimized) model.pipelib_minimized = true;
    lint_render();
};

export const text_editor_set_minimized = value => {
    model.text_editor_minimized = value;
    lint_render();
};

export const text_editor_set_readonly = value => {
    model.text_editor_readonly = value;
    window.render_app();
};

export const pipelib_toggle_minimized = () => {
    model.pipelib_minimized = !model.pipelib_minimized;
    if (!model.pipelib_minimized) model.text_editor_minimized = true;
    lint_render();
};

export const pipelib_set_minimized = value => {
    model.pipelib_minimized = value;
    lint_render();
};

export const job_editor_set_name = name => {
    model.job_editor_name = name;
    lint_render();
};

export const pipeline_text_editor_save = text => {
    model.pipeline = yaml.safeLoad(text);
    lint_render();
};

export const pipeline_reorder_stage = (current_index, new_index) => {
    const original = model.pipeline.stages;
    let stages = model.pipeline.stages;

    if (new_index < current_index) {
        stages = [...stages.slice(0, new_index), stages[current_index], ...stages.slice(new_index)];
        stages.splice(current_index + 1, 1);
    }
    else if (new_index > current_index) {
        stages = [
            ...original.slice(0, current_index),
            ...original.slice(current_index + 1, new_index),
            original[current_index],
            ...original.slice(new_index),
        ];
    }

    model.pipeline.stages = stages;
    lint_render();
};

export const pipeline_create_stage = () => {
    const name = random_words(2).join(' ');
    model.pipeline.stages.push(name);
    lint_render();
};

export const pipeline_remove_stage = index => {
    model.pipeline.stages.splice(index, 1);
    lint_render();
};

export const pipeline_update_stage = (index, new_name) => {
    const stage = model.pipeline.stages[index];
    const jobs = get_jobs_for_stage(stage);
    jobs.forEach(job => {
        const key = job.name;
        model.pipeline[key].stage = new_name;
    });
    model.pipeline.stages[index] = new_name;
    lint_render();
};

export const pipeline_create_job = stage => {
    let name = random_words(2).join(' ');
    while (model.pipeline[name]) name = random_words(2).join(' ');
    model.pipeline[name] = { stage, script: ['echo "hello, world!"'] };
    model.job_editor_name = name;
    lint_render();
};

export const pipeline_delete_job = name => {
    // todo validate deletion, for example is target needed by another job?
    delete model.pipeline[name];
    job_editor_set_name(null);
    lint_render();
};

export const pipeline_update_job_name = (current_name, new_name) => {
    model.job_editor_name = new_name;
    model.pipeline[new_name] = model.pipeline[current_name];
    delete model.pipeline[current_name];
    lint_render();
};

export const pipeline_set_job_stage = (job_name, stage_index) => {
    model.pipeline[job_name].stage = model.pipeline.stages[stage_index];
    lint_render();
};

export const pipeline_set_job_image = (job_name, value) => {
    if (value) model.pipeline[job_name].image = value;
    else delete model.pipeline[job_name].image;
    lint_render();
};

export const pipeline_set_job_resource_group = (job_name, resource_group) => {
    model.pipeline[job_name].resource_group = resource_group;
    lint_render();
};

export const pipeline_set_job_services = (job_name, value) => {
    model.pipeline[job_name].services = value;
    lint_render();
};

export const pipeline_set_job_before_script = (job_name, value) => {
    model.pipeline[job_name].before_script = value;
    lint_render();
};

export const pipeline_set_job_script = (job_name, value) => {
    model.pipeline[job_name].script = value;
    lint_render();
};

export const pipeline_set_job_after_script = (job_name, value) => {
    model.pipeline[job_name].after_script = value;
    lint_render();
};

export const pipeline_set_job_needs = (job_name, value) => {
    model.pipeline[job_name].needs = value;
    lint_render();
};

export const pipeline_set_job_extends = (job_name, value) => {
    model.pipeline[job_name].extends = value;
    lint_render();
};

export const pipeline_set_job_environment_name = (job_name, value) => {
    if (!model.pipeline[job_name].environment) {
        model.pipeline[job_name].environment = {};
    }
    if (value) {
        model.pipeline[job_name].environment.name = value;
    }
    else {
        model.pipeline[job_name].environment = null;
    }
    lint_render();
};

export const pipeline_set_job_environment_url = (job_name, value) => {
    model.pipeline[job_name].environment.url = value;
    lint_render();
};

export const pipeline_set_job_environment_on_stop = (job_name, value) => {
    model.pipeline[job_name].environment.on_stop = value;
    lint_render();
};

export const pipeline_set_job_environment_action = (job_name, value) => {
    model.pipeline[job_name].environment.action = value;
    lint_render();
};

export const pipeline_set_job_environment_auto_stop_in = (job_name, value) => {
    model.pipeline[job_name].environment.auto_stop_in = value;
    lint_render();
};

export const pipeline_set_job_environment_kubernetes_namespace = (job_name, value) => {
    model.pipeline[job_name].environment.kubernetes = { namespace: value };
    lint_render();
};

export const pipeline_set_job_trigger_branch = (job_name, value) => {
    model.pipeline[job_name].trigger = model.pipeline[job_name].trigger || {};
    model.pipeline[job_name].trigger.branch = value;
    lint_render();
};

export const pipeline_set_job_trigger_strategy = (job_name, value) => {
    model.pipeline[job_name].trigger = model.pipeline[job_name].trigger || {};
    model.pipeline[job_name].trigger.strategy = value;
    lint_render();
};

export const pipeline_set_job_trigger_include = (job_name, value) => {
    model.pipeline[job_name].trigger = model.pipeline[job_name].trigger || {};
    model.pipeline[job_name].trigger.include = value;
    lint_render();
};

export const pipeline_set_job_trigger_project = (job_name, value) => {
    model.pipeline[job_name].trigger = model.pipeline[job_name].trigger || {};
    model.pipeline[job_name].trigger.project = value;
    lint_render();
};

export const pipeline_set_job_trigger = (job_name, value) => {
    model.pipeline[job_name].trigger = value;
    lint_render();
};

export const pipeline_set_job_coverage = (job_name, value) => {
    model.pipeline[job_name].coverage = value;
    lint_render();
};

export const pipeline_set_job_parallel = (job_name, value) => {
    model.pipeline[job_name].parallel = value;
    lint_render();
};

export const pipeline_set_job_retry = (job_name, value) => {
    model.pipeline[job_name].retry = value;
    lint_render();
};

export const pipeline_set_job_timeout = (job_name, value) => {
    model.pipeline[job_name].timeout = value;
    lint_render();
};

export const pipeline_set_job_interruptible = (job_name, value) => {
    if (value) {
        model.pipeline[job_name].interruptible = value;
    }
    else {
        delete model.pipeline[job_name].interruptible;
    }
    lint_render();
};

export const pipeline_set_job_cache = (job_name, value) => {
    model.pipeline[job_name].cache = value;
    lint_render();
};

export const pipeline_set_job_artifacts = (job_name, value) => {
    model.pipeline[job_name].artifacts = value;
    lint_render();
};

export const pipeline_set_job_release = (job_name, value) => {
    if (value) {
        if (!value.tag_name || value.tag_name === '') delete value.tag_name;
        if (!value.name || value.name === '') delete value.name;
        if (!value.description || value.description === '') delete value.description;
        if (!value.ref || value.ref === '') delete value.ref;
        if (!value.milestones || value.milestones.length === 0) delete value.milestones;
        model.pipeline[job_name].release = value;
    }
    else delete model.pipeline[job_name].release;
    lint_render();
};

export const pipeline_set_job_rules = (job_name, value) => {
    model.pipeline[job_name].rules = value;
    lint_render();
};

export const pipeline_set_job_variables = (job_name, value) => {
    model.pipeline[job_name].variables = value;
    lint_render();
};

export default model;

// https://json.schemastore.org/gitlab-ci
